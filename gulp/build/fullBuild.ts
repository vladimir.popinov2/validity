import { parallel, series } from "gulp";
import {
  addEs5ImportTarget, addEs6ImportTarget, compileTypescript, compileTypescriptEs5,
  compileTypescriptEs6, removeTempEs5Folder, removeTempEs6Folder
} from "./parts";
import { preparation } from "./preparation";

const es5 = series(compileTypescriptEs5, addEs5ImportTarget, removeTempEs5Folder);
const es6 = series(compileTypescriptEs6, addEs6ImportTarget, removeTempEs6Folder);

export const fullBuild = series(
  preparation,
  parallel(compileTypescript, es5, es6)
);
