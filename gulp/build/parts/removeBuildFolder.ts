import { settings } from "../../settings";
import { removeDir } from "../../tools/removeDir";

export function removeBuildFolder(): Promise<void> {
  return removeDir(`./${settings.build}`);
}

