import { removeDir } from "../../tools/removeDir";
import { settings } from "../../settings";

function removeTempFolder(folder: string): Promise<void> {
  return removeDir(folder + "temp");
}

export function removeTempEs5Folder(): Promise<void> {
  return removeTempFolder(settings.buildEs5);
}

export function removeTempEs6Folder(): Promise<void> {
  return removeTempFolder(settings.buildEs6);
}
