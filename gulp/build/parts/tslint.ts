import { spawn } from "@vlr/spawn";

export function tslint(): Promise<void> {
  return spawn("tslint", "tslint", "-p", "tsconfig.json");
}
