import { transform } from "@vlr/gulp-transform";
import { generate, RazorOptions } from "@vlr/razor";
import * as gulp from "gulp";
import { src } from "../settings/srcFiles";

export function razor(): NodeJS.ReadWriteStream {
  const razorFiles = src() + "/**/*.rzr";
  const options: RazorOptions = {
    quotes: '"',
    linefeed: "\n",
    language: "ts"
  };
  return gulp.src(razorFiles)
    .pipe(transform(file => ({ ...file, contents: generate(file.contents, options), extension: ".ts" })))
    .pipe(gulp.dest(src()));
}
