import { generateBuckets, GeneratorOptions } from "@vlr/bucket-gen";
import { transformRange } from "@vlr/gulp-transform";
import * as gulp from "gulp";
import { src, typesAndCtors } from "../settings/srcFiles";

export function buckets(): NodeJS.ReadWriteStream {
  const options: GeneratorOptions = {
    quotes: "\"",
    linefeed: "\n",
  };

  return gulp.src(typesAndCtors())
    .pipe(transformRange((files) => generateBuckets(files, options)))
    .pipe(gulp.dest(src()));
}
