import { series, parallel } from "gulp";
import { typeConstructors } from "./typeConstructors";
import { buckets } from "./buckets";
import { razor } from "./razor";

export const automate = parallel(
  series(typeConstructors, buckets),
  razor
);
