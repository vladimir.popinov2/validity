import { transform, changeFileType } from "@vlr/gulp-transform";
import { GeneratorOptions, generate } from "@vlr/type-constructor-gen";
import * as gulp from "gulp";
import { types, src } from "../settings/srcFiles";

export function typeConstructors(): NodeJS.ReadWriteStream {
  const options: GeneratorOptions = {
    quotes: "\"",
    linefeed: "\n",
  };

  return gulp.src(types())
    .pipe(transform((file) => ({
      ...file,
      contents: generate(file.contents, file.name, options),
      name: changeFileType(file.name, ".ctor"),
    })))
    .pipe(gulp.dest(src()));
}
