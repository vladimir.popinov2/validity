import { isEmptyValue } from "../helpers";
import { createMessage } from "../helpers/createMessage";
import { ItemValidation, messages, ValidatorFunc, ChildValidatorFunc, Predicate, ChildPredicate } from "../types";

export function required<T>(message?: string, isEnabled?: Predicate<T>): ValidatorFunc<T>;
export function required<T, TP>(message?: string, isEnabled?: ChildPredicate<T, TP>): ChildValidatorFunc<T, TP>;
export function required<T, TP>(message?: string, isEnabled?: ChildPredicate<T, TP>): ChildValidatorFunc<T, TP> {
  const _messages = [createMessage(message || messages.required)];
  isEnabled = isEnabled != null ? isEnabled : () => true;
  return function (_value: T, _show: boolean, parent: TP): ItemValidation<T> {
    if (!isEnabled(_value, parent)) {
      return { _value, _valid: true, _show };
    }

    return isEmptyValue(_value)
      ? { _value, _valid: false, _required: true, _show, _messages }
      : { _value, _valid: true, _required: true, _show };
  };
}
