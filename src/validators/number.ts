import { isNumber } from "@vlr/object-tools";
import { validator } from "../helpers/validator";
import { messages, ValidatorFunc } from "../types";
import { createMessage } from "../helpers/createMessage";

export function number(message?: string): ValidatorFunc<number> {
  return validator<number>(_value => isNumber(_value), createMessage(message || messages.number));
}
