import { Validator, ValidatorFunc } from "../types";
import { validate } from "../validate";

export function and<T>(...validators: Validator<T>[]): ValidatorFunc<T> {
  return (val: T, show: boolean) => validate(validators, val, show);
}
