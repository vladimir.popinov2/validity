import { isString } from "@vlr/object-tools";
import { validator } from "../helpers/validator";
import { Message, ValidatorFunc, messages } from "../types";
import { createMessage } from "../helpers/createMessage";

export const numeric = (message?: string) => regex(/^[0-9]*$/, message || messages.numeric);

export const alpha = (message?: string) => regex(/^[a-zA-Z]*$/, message || messages.alpha);

export const alphanumeric = (message?: string) => regex(/^[a-zA-Z0-9]*$/, message || messages.alphanumeric);

export function regex(reg: RegExp, message?: string): ValidatorFunc<string> {
  const msg: Message = createMessage(message || messages.regex);
  return validator(match(reg), msg);
}

function match(reg: RegExp): (_value: string) => boolean {
  return (_value: string) => !isString(_value) || !!_value.match(reg);
}
