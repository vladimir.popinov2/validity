import { isString } from "@vlr/object-tools";

export function isEmptyValue(value: any): boolean {
  return value == null || (isString(value) && value.trim() === "");
}
