import { Predicate, ChildPredicate } from "../types/predicate.type";
import { ValidatorFunc, Message, ChildValidatorFunc, ItemValidation } from "../types";

export function requiredValidator<T>(predicate: Predicate<T>, message: Message): ValidatorFunc<T>;
export function requiredValidator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message): ChildValidatorFunc<T, TP>;
export function requiredValidator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message): ChildValidatorFunc<T, TP> {
  const _messages = [message];
  return function (_value: T, _show: boolean, parent: TP): ItemValidation<T> {
    return predicate(_value, parent)
      ? { _value, _valid: true, _required: true, _show }
      : { _value, _valid: false, _required: true, _show, _messages };
  };
}
