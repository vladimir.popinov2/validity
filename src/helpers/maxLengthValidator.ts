import { Message, ItemValidation, ValidatorFunc, ChildValidatorFunc } from "../types";
import { Predicate, ChildPredicate } from "../types/predicate.type";
import { isEmptyValue } from "./isEmptyValue";

export function maxLengthValidator<T>(predicate: Predicate<T>, message: Message, _maxLength: number): ValidatorFunc<T>;
export function maxLengthValidator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message, _maxLength: number): ChildValidatorFunc<T, TP>;
export function maxLengthValidator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message, _maxLength: number): ChildValidatorFunc<T, TP> {
  const _messages = [message];
  return function (_value: T, _show: boolean, parent: TP): ItemValidation<T> {
    return isEmptyValue(_value) || predicate(_value, parent)
      ? { _value, _valid: true, _maxLength, _show }
      : { _value, _valid: false, _maxLength, _show, _messages };
  };
}
