import { Message, ItemValidation, ValidatorFunc, ChildValidatorFunc } from "../types";
import { Predicate, ChildPredicate } from "../types/predicate.type";
import { isEmptyValue } from "./isEmptyValue";
import { isString } from "@vlr/object-tools";
import { createMessage } from "./createMessage";

export function validator<T>(predicate: Predicate<T>, message: Message | string): ValidatorFunc<T>;
export function validator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message | string): ChildValidatorFunc<T, TP>;
export function validator<T, TP>(predicate: ChildPredicate<T, TP>, message: Message | string): ChildValidatorFunc<T, TP> {
  const _messages = isString(message) ? [createMessage(message)] : [message];
  return function (_value: T, _show: boolean, parent: TP): ItemValidation<T> {
    const _valid = isEmptyValue(_value) || predicate(_value, parent);
    return _valid
      ? { _value, _valid, _show }
      : { _value, _valid, _messages, _show };
  };
}
