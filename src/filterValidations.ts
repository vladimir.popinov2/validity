import { ItemValidation } from "./types";
import { findIndex } from "@vlr/array-tools";

export interface DisableValidation<T> extends ItemValidation<T> {
  disabled: boolean;
}

export function filterValidations<T>(validations: ItemValidation<T>[]): ItemValidation<T>[] {
  let index = findIndex(validations, (item: DisableValidation<T>) => item.disabled);
  const enabledValidators = index > -1 ? validations.slice(0, index) : validations;
  // filter out all "enabledIf" results;
  return enabledValidators.filter((x: DisableValidation<T>) => x.disabled == null);
}
