import { Validation } from "./validation.type";

export type Validate<T> = (obj: T) => Validation<T>;
