export type Predicate<T> = (_value: T) => boolean;
export type ChildPredicate<T, TP> = (_value: T, parent: TP) => boolean;
