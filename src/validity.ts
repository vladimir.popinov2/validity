import { Validators } from "./types/validator.type";
import { Validate } from "./types/validate.type";
import { validate } from "./validate";

export function validity<T>(validators: Validators<T>, show: boolean = true): { validate: Validate<T> } {
  return { validate: (obj: T) => validate(validators, obj, show) };
}
