import { expect } from "chai";
import { required } from "../src/validators/required";
import { expectMessage, customMessage, expectCustom } from "./testSupport";
import { messages } from "../src";

describe("required", function (): void {
  it("should return valid for constant", function (): void {
    // arrange

    // act
    const result = required()(1, true);

    // assert
    expect(result._valid).equals(true);
    expect(result._required).equals(true);
  });

  it("should return invalid for blank string", function (): void {
    // arrange

    // act
    const result = required(customMessage)("   ", true);

    // assert
    expect(result._valid).equals(false);
    expect(result._required).equals(true);
    expectCustom(result);
  });

  it("should return invalid for null", function (): void {
    // arrange

    // act
    const result = required()(null, true);

    // assert
    expect(result._valid).equals(false);
    expect(result._required).equals(true);
    expect(result._show).equals(true);
  });

  it("should set show flag", function (): void {
    // arrange

    // act
    const result = required()(null, false);

    // assert
    expect(result._show).equals(false);
    expectMessage(result, messages.required);
  });

  it("should set custom message", function (): void {
    // arrange

    // act
    const result = required(customMessage)(null, false);

    // assert
    expect(result._show).equals(false);
    expectCustom(result);
  });

  it("should not set required if isEnabled is false", function (): void {
    // arrange

    // act
    const result = required(customMessage, () => false)(null, false);

    // assert
    expect(!!result._required).equals(false);
    expect(result._valid).equals(true);
    expect(result._show).equals(false);
    expect(result._messages == null).equals(true);
  });
});
