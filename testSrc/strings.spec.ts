import { expect } from "chai";
import { numeric, alphanumeric, alpha, regex } from "../src/validators/strings";
import { customMessage, expectCustom, expectMessage } from "./testSupport";
import { messages } from "../src";

describe("numeric", function (): void {
  it("should return valid for digits", function (): void {
    // arrange

    // act
    const result = numeric()("234", true);

    // assert
    expect(result._valid).equals(true);

  });

  it("should return invalid for letters", function (): void {
    // arrange

    // act
    const result = numeric()("asdf2213", true);

    // assert
    expect(result._valid).equals(false);
    expectMessage(result, messages.numeric);
  });

  it("should return return custom message", function (): void {
    // arrange

    // act
    const result = numeric(customMessage)("asdf2213", true);

    // assert
    expectCustom(result);
  });
});

describe("alpha", function (): void {
  it("should return valid for letters", function (): void {
    // arrange

    // act
    const result = alpha()("abhy", true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for digits", function (): void {
    // arrange

    // act
    const result = alpha()("asdf2213", true);

    // assert
    expect(result._valid).equals(false);
    expectMessage(result, messages.alpha);
  });

  it("should return custom message", function (): void {
    // arrange

    // act
    const result = alpha(customMessage)("asdf2213", true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });
});

describe("alphanumeric", function (): void {
  it("should return valid for letters and digits", function (): void {
    // arrange

    // act
    const result = alphanumeric()("abhy123", true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for symbols", function (): void {
    // arrange

    // act
    const result = alphanumeric()("asd&f22$13", true);

    // assert
    expect(result._valid).equals(false);
    expectMessage(result, messages.alphanumeric);
  });

  it("should return custom message", function (): void {
    // arrange

    // act
    const result = alphanumeric(customMessage)("asd&f22$13", true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });
});

describe("regex", function (): void {
  it("should return valid for match", function (): void {
    // arrange

    // act
    const result = regex(/^[a-c]*$/)("abc", true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for non-match", function (): void {
    // arrange

    // act
    const result = regex(/^[a-c]*$/)("cde", true);

    // assert
    expect(result._valid).equals(false);
    expectMessage(result, messages.regex);
  });

  it("should return custom message", function (): void {
    // arrange

    // act
    const result = regex(/^[a-c]*$/, customMessage)("cde", true);

    // assert
    expect(result._valid).equals(false);
    expectCustom(result);
  });
});
