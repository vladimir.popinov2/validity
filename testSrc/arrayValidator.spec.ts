import { expect } from "chai";
import { validNum, invalidNum } from "./testSupport";
import { arrayValidator } from "../src/validators/arrayValidator";
import { required } from "../src/validators/required";

describe("arrayValidator", function (): void {
  it("should return valid if array is null", function (): void {
    // arrange

    // act
    const result = arrayValidator(invalidNum)(null, true);

    // assert
    expect(result._valid).equals(true);

  });
  it("should return valid for empty array", function (): void {
    // arrange

    // act
    const result = arrayValidator(invalidNum)([], true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return valid if all items are valid", function (): void {
    // arrange

    // act
    const result = arrayValidator(validNum)([1, 2, 3], true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return array of validations", function (): void {
    // arrange
    const val = (input: number) => ({ _value: 1, _valid: input !== 2, _show: true });
    // act
    const result = arrayValidator(val)([1, 2, 3], true);

    // assert
    expect(result._valid).equals(false);
    expect(result._show).equal(true);
    expect(result._items.map(v => v._valid)).deep.equals([true, false, true]);
    expect(result._items.map(v => v._show)).deep.equals([false, true, false]);
  });

  it("should set show flag", function (): void {
    // arrange

    // act
    const result = arrayValidator(required())([], false);

    // assert
    expect(result._show).equals(false);
  });
});
