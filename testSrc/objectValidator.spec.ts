import { expect } from "chai";
import { validity, required, Validator } from "../src";
import { validate } from "../src/validate";
import { invalidNum, validBool, validNum } from "./testSupport";

interface TP {
  bool: boolean;
  num: number;
}

describe("objectValidator", function (): void {
  it("should return valid if obj is null", function (): void {
    // arrange
    const tpVal = { num: validNum };
    // act
    const result = validate<TP>(tpVal, null, true);

    // assert
    expect(result._valid).equals(true);
    expect(result._show).equals(false);
  });

  it("should return objectValidation", function (): void {
    // arrange
    const tpVal = {
      bool: validBool,
      num: invalidNum
    };

    // act
    const result = validity<TP>(tpVal).validate(<any>{});

    // assert
    expect(result.bool._valid).equals(true);
    expect(result.num._valid).equals(false);
    expect(result._valid).equals(false);
  });

  it("should return valid if all fields are valid", function (): void {
    // arrange
    const tpVal = {
      bool: validBool,
      num: validNum
    };
    // act
    const result = validate<TP>(tpVal, <any>{});

    // assert
    expect(result._valid).equals(true);
  });

  it("should handle array validator", function (): void {
    // arrange
    const tpVal = {
      num: [validNum, validNum]
    };

    // act
    const result = validate<TP>(tpVal, <any>{});

    // assert
    expect(result._valid).equals(true);
  });

  it("should set show to true", function (): void {
    // arrange
    const tpVal: Validator<TP> = {
      bool: required()
    };
    // act
    const result = validate<TP>(tpVal, <any>{}, true);

    // assert
    expect(result._show).equals(true);
    expect(result.bool._show).equals(true);
  });

  it("should set show to false", function (): void {
    // arrange
    const tpVal: Validator<TP> = {
      bool: required()
    };
    // act
    const result = validate<TP>(tpVal, <any>{}, false);

    // assert
    expect(result._show).equals(false);
    expect(result.bool._show).equals(false);
  });
});
