import { expect } from "chai";
import { validity } from "../src/validity";
import { validate } from "../src/validate";
import { validNum, invalidNum } from "./testSupport";
import { lessThan, greaterThan, messages, Message, createMessage } from "../src";

describe("validate", function (): void {
  it("should work with result of a single validator", function (): void {
    // arrange
    const vresult = { _value: 1, _valid: true, _show: true };

    // act
    const result = validity(() => vresult).validate(1);

    // assert
    expect(result).deep.equals({ _value: 1, _valid: true, _show: false });
  });

  it("should return valid for empty array of validators", function (): void {
    // arrange

    // act
    const result = validate([], 1);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return false if one is false", function (): void {
    // arrange

    // act
    const result = validate([validNum, invalidNum, validNum], 1);

    // assert
    expect(result._valid).equals(false);
  });

  it("should return extra flag", function (): void {
    // arrange
    const vresult = { _value: 1, _valid: true, somefield: 77, _show: true };

    // act
    const result = validity([validNum, () => vresult], true).validate(1);

    // assert
    expect(result._valid).equals(true);
    expect(result._show).equals(false);
    expect((<any>result).somefield).equals(77);
  });

  it("should return required if one is required", function (): void {
    // arrange
    const vresult = { _value: 1, _valid: true, _required: true, _show: true };

    // act
    const result = validity([validNum, () => vresult, validNum]).validate(1);

    // assert
    expect(result._required).equals(true);
    expect(result._maxLength).equals(undefined);
  });

  it("should return maxLength", function (): void {
    // arrange
    const vresult = { _value: 1, _valid: true, _maxLength: 10, _show: true };

    // act
    const result = validity([validNum, () => vresult], false).validate(1);

    // assert
    expect(result._maxLength).equals(10);
  });

  it("should compile messages", function (): void {
    // arrange
    const validator = [greaterThan(12), lessThan(10)];

    // act
    const result = validate(validator, 11, false);

    // assert
    const expected: Message[] = [
      createMessage(messages.greaterThan, 12),
      createMessage(messages.lessThan, 10)
    ];
    expect(result._messages).deep.equals(expected);
  });
});
