import { number } from "../src/validators/number";
import { expect } from "chai";

describe("number", function (): void {
  it("should return valid for null", function (): void {
    // arrange

    // act
    const result = number()(null, true);

    // assert
    expect(result._valid).equals(true);
  });

  it("should return invalid for string", function (): void {
    // arrange

    // act
    const result = number()(<any>"5", true);

    // assert
    expect(result._valid).equals(false);
  });

  it("should return valid for number", function (): void {
    // arrange

    // act
    const result = number()(5, true);

    // assert
    expect(result._valid).equals(true);
  });
});
