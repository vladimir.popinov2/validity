import { validNum, invalidNum } from "./testSupport";
import { and } from "../src";
import { expect } from "chai";

describe("and", function (): void {
  it("should return valid if all are valid", function (): void {
    // arrange

    // act
    const result1 = and(validNum, validNum, validNum)(1, true);

    // assert
    expect(result1._valid).equals(true);
  });

  it("should return invalid if one is valid", function (): void {
    // arrange

    // act
    const result1 = and(validNum, invalidNum, validNum)(1, true);

    // assert
    expect(result1._valid).equals(false);
  });
});
